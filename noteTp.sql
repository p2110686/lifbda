%%sql
-- Script de création de la base du club de danse 

rollback;
drop schema if exists club_de_danse cascade;
create schema club_de_danse;
set search_path to club_de_danse;

--Entité professeur
create table professeurs(
	num_prof 		integer primary key,
	nom_prof		text,
	prenom_prof		text,
	tel_prof		text
	);

-- Entité salariés spécialisation de professeurs (clé primaire et étrangère à la fois)
create table salaries(
	num_salarie 			integer primary key,
	dateembauche_salarie	date,
	echelon_salarie			integer,
	salaire_salarie			decimal,

	constraint fk_sal_prof foreign key (num_salarie) references professeurs(num_prof) on delete cascade on update cascade
	-- Si un professeur salarie est supprimé ou modifié dans professeurs, il est supprimé ou modifié dans salaries
);

-- Entité vacataires spécialisation de professeurs (clé primaire et étrangère à la fois)

create table vacataires(
	num_vacataire 				integer primary key,
	statut_vacataire			text, -- par exemple auto-entrepreneur, étudiant, sans-emploi...
	constraint fk_vac_prof foreign key (num_vacataire) references professeurs(num_prof) on delete cascade on update cascade
	-- Si un professeur vacataire est supprimé ou modifié dans professeurs, il est supprimé ou modifié dans vacataires

);

-- Entité Contrat, entité faible de Vacataire. L'identifiant local (pour un vacataire donné) est la date du contrat.
create table contrats(
	num_vacataire 		integer references vacataires(num_vacataire) on delete cascade on update cascade,
-- Si un vacataire est supprimé, tous ses contrats seront supprimés.
	date_contrat		date,
	salaire_h_contrat	decimal,
	
	primary key (num_vacataire,date_contrat)

);

-- La table des cours. Contient l'association (1-N) 'Est Responsable' sous la forme d'une clé étrangère vers
-- le professeur responsable. La participation de chaque cours ) cette association est obligatoire (NOT NULL).
create type niveau as enum('Débutant','Avancé','Expert');
create table cours(
	code_cours				integer primary key,
	intitule_cours			text,
	numresponsable_cours	integer not null references professeurs(num_prof) on update cascade,
-- ici, pas de 'on update delete' : si un professeur est supprimé, on ne supprime pas les cours dont il est responsable.
-- donc la suppression d'un professeur sera interdite s'il est responsable d'au moins un cours.
	niveau_cours			niveau
);


-- Table qui traduit l'association (N-N) 'Intervient' entre les professeurs et les cours.
create table prof_intervient_cours(
	num_prof		integer references professeurs(num_prof) on update cascade on delete cascade,
	code_cours		integer references cours(code_cours) on update cascade on delete cascade,
-- Si un prof est supprimé, ses interventions seront supprimées d'office.
-- Si un cours est supprimé, les interventions le concernant son supprimées d'office.

	primary key(num_prof,code_cours)

);

**Professeur**(<u>num</u>, nom, prenom, telephone)

**Salarie**(<u>num</u>, date_embauche, echelon, salaire_mensuel)

**Vacataire**(<u>num</u>, statut)
**Cours**(<u>code</u>, intitule, niveau, #responsable_num)

**Contrat**(<u>date_contrat</u>, <u>salaire_horaire</u>, #num_professeur)

**Intervient**(#num_professeur, #code_cours)

%%sql
ALTER TABLE cours
ADD CONSTRAINT fk_responsable
FOREIGN KEY (numresponsable_cours)
REFERENCES professeurs(num_prof);

%%sql
INSERT INTO cours (code_cours, intitule_cours, numresponsable_cours, niveau_cours) VALUES (1, 'Salsa Débutant', 1, 'Débutant');

/*test 01
error car c'est circulaire
*/

/*
test 02

it's ok 

la question 3 vient resoudre le probleme
*/

%%sql
ALTER TABLE prof_intervient_cours
  ADD CONSTRAINT fk_prof FOREIGN KEY (num_prof) REFERENCES Professeurs (num_prof) DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE Cours
  ADD CONSTRAINT fk_responsable FOREIGN KEY (numresponsable_cours) REFERENCES Professeurs (num_prof) DEFERRABLE INITIALLY DEFERRED;

%%sql
ROLLBACK;
BEGIN;
SET CONSTRAINTS ALL DEFERRED;
INSERT INTO Professeurs (num_prof, nom_prof, prenom_prof, tel_prof) VALUES (1, 'Jean', 'Wiejacka', '0102030405');
INSERT INTO prof_intervient_cours (num_prof, code_cours) VALUES (1, 101);
INSERT INTO Cours (code_cours, intitule_cours, niveau, numresponsable_cours) VALUES (101, 'Salsa Débutant', 'Débutant', 1);
COMMIT;




    3.Une transaction est un ensemble de mises à jour considérées comme une seule opération, sans limite sur le nombre d'opérations dans une même transaction. Les SGBD relationnels garantissent que les contraintes sont satisfaites AVANT et APRES la transaction. Ce qui se passe PENDANT la transaction dépend des capacités des SGBD et des choix de l'opérateur. La vérification de certaines contraintes, comme les clés étrangères sous PostgreSQL, peut être différée à l'issue de la transaction.
        3.A.Modifiez, lorsque cela est nécessaire, les déclarations de clés étrangères afin de différer leur vérification en fin de transaction.
        3.B.Procédez maintenant à la création du cours de SALSA à l'aide d'une transaction contenant plusieurs insertions de tuples.

    4.Le cahier des charges spécifie qu'un cours a obligatoirement (au moins) un intervenant (participation obligatoire à l'association "Intervient"). Peut-on considérer que cette contrainte est bien garantie dans notre base de données ?

    5.Implanter la contrainte X

    indiquée dans la spécialisation des professeurs, indiquant qu'un professeur ne peut pas être à la fois un salarié et un vacataire.

    Indication : Cette contrainte peut se programmer à l'aide de deux commandes "CHECK" dans la déclaration des relations "salaries" et "vacataires", par exemple sous la forme "Check (est_vacataire(num_prof) = false)" - où "est_vacataire(num_prof)" est une fonction qui retourne vrai si "num_prof" existe déjà dans vacataires, faux sinon.

Voici pour servir d'exemple une proposition de code de création d'une fonction qui renvoie VRAI ssi le numéro passé en argument est celui d'un vacataire.

drop function if exists est_vacataire;
CREATE function est_vacataire(un_num_prof integer) returns boolean
as $$
begin
 	perform num_vacataire  
 	from vacataires 
 	where num_vacataire = un_num_prof;
 
 	return (found);
end;
 $$ language plpgsql;

    6.Implanter la contrainte T

    de la spécialisation, qui impose que tous les professeurs ont la position de salarié ou de vacataire.
        6.A.Lors de la création d'un professeur : ajouter une contrainte "check" qui s'assure que ce professeur est bien un salarié ou un vacataire.
        6.B.Différez les contraintes de clés étrangères dans "salariés" et "vacataires", de façon à pouvoir momentanément créer un salarié ou vacataire qui n'est pas un professeur.
        6.C.Effectuez alors l'insertion d'un nouveau professeur - vacataire dans une transaction qui crée d'abord le vacataire pour le professeur.
        
7.Ce n'est pas tout à fait fini ! Il faut encore s'assurer que personne ne puisse supprimer une ligne des relations "vacataires" ou "salariés" car cela contredirait la contrainte T (un professeur serait ni salarié, ni vacataire). Mais si on fait une contrainte trop forte qui interdit ces suppressions, on ne pourra plus faire les opérations suivantes : modifier un professeur en le passant de vacataire à salarié (ou l'inverse), ou même supprimer un professeur - car Postgre va tenter de supprimer le vacataire/salarié correspondant (en cascade). Voici comment résoudre ces problèmes.

    7.A Créer une fonction est_prof qui teste si un numéro de professeur (fourni en entrée) existe parmi les professeurs.
	7.B Faire un trigger (https://doc.postgresql.fr/16/sql-createtrigger.html) différable sur "salaries" qui interdit de supprimer un salarié si celui est un professeur qui existe encore et qu'il n'est pas dans "vacataires".
        7.C Faire un trigger différable sur "vacataires" qui interdit de supprimer un vacataire si celui est un professeur qui existe encore et qu'il n'est pas dans "salaries".
        7.DVérifiez maintenant que vous pouvez 1) supprimer un professeur et 2) passer un professeur de salarié à vacataire en utilisant une transaction à deux étapes.

Aide : comment faire le trigger sur salariés :

-- Il faut d'abord faire une fonction qui sera utilisée par le trigger : elle ne doit pas prendre de paramètres, et retourner un objet de type 'trigger'.
drop function if exists check_prof_vacataire cascade; 
CREATE FUNCTION check_prof_vacataire() RETURNS trigger 
as $$
begin
	if  not est_prof(old.num_salarie) then return null; -- le prof a été supprimé (en cours de suppression)
	elsif est_vacataire(old.num_salarie) then return null; -- le salarié est bien dans vacataires.
	else raise exception 'professeur doit etre vacataire si plus salarie';
	end if;
end;
$$ LANGUAGE plpgsql;

create constraint trigger check_prof_vacataire
	after delete or update on salaries deferrable for each row execute function check_prof_vacataire();



/*****************  my *********************/
3.A

ALTER TABLE prof_intervient_cours
ADD CONSTRAINT fk_prof
FOREIGN KEY (num_prof) REFERENCES Professeurs (num_prof)
DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE Cours
ADD CONSTRAINT fk_responsable
FOREIGN KEY (numresponsable_cours) REFERENCES Professeurs (num_prof)
DEFERRABLE INITIALLY DEFERRED;

3.B
BEGIN;
SET CONSTRAINTS ALL DEFERRED;

INSERT INTO Professeurs (num_prof, nom_prof, prenom_prof, tel_prof)
VALUES (1, 'Jean', 'Wiejacka', '0102030405');

INSERT INTO prof_intervient_cours (num_prof, code_cours)
VALUES (1, 101);

INSERT INTO Cours (code_cours, intitule_cours, niveau, numresponsable_cours)
VALUES (101, 'Salsa Débutant', 'Débutant', 1);

COMMIT;

5.

ALTER TABLE salaries
ADD CONSTRAINT check_sal_vac CHECK (
  NOT EXISTS (SELECT 1 FROM vacataires WHERE num_vacataire = salaries.num_salarie)
);

ALTER TABLE vacataires
ADD CONSTRAINT check_vac_sal CHECK (
  NOT EXISTS (SELECT 1 FROM salaries WHERE num_salarie = vacataires.num_vacataire)
);

6.A
ALTER TABLE salaries
ADD CONSTRAINT check_sal_vac CHECK (
  NOT EXISTS (SELECT 1 FROM vacataires WHERE num_vacataire = salaries.num_salarie)
);

ALTER TABLE vacataires
ADD CONSTRAINT check_vac_sal CHECK (
  NOT EXISTS (SELECT 1 FROM salaries WHERE num_salarie = vacataires.num_vacataire)
);

6.B
ALTER TABLE salaries
ADD CONSTRAINT fk_sal_prof DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE vacataires
ADD CONSTRAINT fk_vac_prof DEFERRABLE INITIALLY DEFERRED;

6.C
BEGIN;
SET CONSTRAINTS ALL DEFERRED;

INSERT INTO vacataires (num_vacataire, statut_vacataire)
VALUES (2, 'Auto-entrepreneur');

INSERT INTO professeurs (num_prof, nom_prof, prenom_prof, tel_prof)
VALUES (2, 'Marie', 'Dupont', '0607080910');

COMMIT;


7.A

DROP FUNCTION IF EXISTS est_prof;
CREATE FUNCTION est_prof(un_num_prof INTEGER) RETURNS BOOLEAN AS $$
BEGIN
  PERFORM num_prof FROM professeurs WHERE num_prof = un_num_prof;
  RETURN FOUND;
END;
$$ LANGUAGE plpgsql;
7.B
DROP FUNCTION IF EXISTS check_prof_salarie;
CREATE FUNCTION check_prof_salarie() RETURNS trigger AS $$
BEGIN
  IF NOT est_prof(OLD.num_salarie) THEN
    RETURN NULL; -- le professeur a été supprimé
  ELSIF est_vacataire(OLD.num_salarie) THEN
    RETURN NULL; -- le salarié est bien dans vacataires
  ELSE
    RAISE EXCEPTION 'professeur doit etre vacataire si plus salarie';
  END IF;
END;
$$ LANGUAGE plpgsql;
7.C
DROP FUNCTION IF EXISTS check_prof_vacataire;
CREATE FUNCTION check_prof_vacataire() RETURNS trigger AS $$
BEGIN
  IF NOT est_prof(OLD.num_vacataire) THEN
    RETURN NULL; -- le professeur a été supprimé
  ELSIF EXISTS (SELECT 1 FROM salaries WHERE num_salarie = OLD.num_vacataire) THEN
    RETURN NULL; -- le vacataire est bien dans salaries
  ELSE
    RAISE EXCEPTION 'professeur doit etre salarie si plus vacataire';
  END IF;
END;
$$ LANGUAGE plpgsql;

CREATE CONSTRAINT TRIGGER check_prof_vacataire
AFTER DELETE OR UPDATE ON vacataires
DEFERRABLE INITIALLY DEFERRED
FOR EACH ROW EXECUTE FUNCTION check_prof_vacataire();

7.D
1. BEGIN;
DELETE FROM salaries WHERE num_salarie = 1;
DELETE FROM professeurs WHERE num_prof = 1;
COMMIT;
2.
BEGIN;
DELETE FROM salaries WHERE num_salarie = 1;
INSERT INTO vacataires (num_vacataire, statut_vacataire) VALUES (1, 'Auto-entrepreneur');
COMMIT;


/******* Tp Re:ingenerie ******************/

%sql duckdb:///
%sql ATTACH 'postgresql://username:passwd@hostname/dbname' AS pg (TYPE POSTGRES);

%%sql duckdb:///
create or replace table elections_brut as select * from 'elections_2022_loire.csv';
create table pg.elections_brut as select * from elections_brut;
COPY pg.elections_brut FROM 'elections_2022_loire.csv' WITH (FORMAT CSV, HEADER, delimiter ';');


%sql COPY pg.elections_brut FROM 'elections_2022_rhone.csv' WITH (FORMAT CSV, HEADER, delimiter ';');

DROP TABLE IF EXISTS elections_brut;
CREATE TABLE elections_brut (
    "Code du département" TEXT,
    "Libellé du département" TEXT,
    "Code de la circonscription" TEXT,
    "Libellé de la circonscription" TEXT,
    "Code de la commune" TEXT,
    "Libellé de la Commune" TEXT,
    "Code du b.vote" TEXT,
    "Inscrits" TEXT,
    "Abstentions" TEXT,
    "% Abs/Ins" TEXT,
    "Votants" TEXT,
    "% Vot/Ins" TEXT,
    "Blancs" TEXT,
    "% Blancs/Ins" TEXT,
    "% Blancs/Vot" TEXT,
    "Nuls" TEXT,
    "% Nuls/Ins" TEXT,
    "% Nuls/Vot" TEXT,
    "Exprimés" TEXT,
    "% Exp/Ins" TEXT,
    "% Exp/Vot" TEXT,
    "N°Panneau" TEXT,
    "Sexe" TEXT,
    "Nom" TEXT,
    "Prénom" TEXT,
    "Voix" TEXT,
    "% Voix/Ins" TEXT,
    "% Voix/Exp" TEXT,
    "Code Officiel EPCI" TEXT,
    "Nom Officiel EPCI" TEXT,
    "Code Officiel Région" TEXT,
    "Nom Officiel Région" TEXT,
    "scrutin_code" TEXT,
    "location" TEXT,
    "lib_du_b_vote" TEXT
);
/* suite le 05 decembre 2024 */

/*  il faut montrer au prof entité association */












