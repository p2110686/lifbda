/*Exercice 1 : création de la base
Vous souhaitez créer un site d’appréciation de films. On gère les films identifiés par un numéro, qui ont
un titre obligatoire, une date de réalisation et un réalisateur. Les évaluateurs ont un identifiant unique et
obligatoirement un nom. Chaque évaluation est définie par le évaluateur, le film et la date d’évaluation ; elle
donne lieu à une note (nombre d’étoiles). Il ne peut pas y avoir d’évaluation sans note.
Précision : pour simplifier, le nom du réalisateur sera une simple information pour chaque film et ne
représentera pas une entité de l’application.
Le schéma Entité/Association ci-dessous est proposé pour représenter ces données.*/
/*
1. Ecrivez un script mocodo dans une cellule de votre notebook pour déclarer ce schéma conceptuel.
Votre cellule commencera par la ligne suivante :
%%mocodo −− s e l e c t mcd −t a r r a n g e −−s v g _ t o p d f −−c o l o r s o c e a n
*/

%%sql
CREATE TABLE Movies (
  PRIMARY KEY (id_Movies  ),
  id_Movies   text NOT NULL,
  title       text,
  "year"      text,
  director    text
);

CREATE TABLE Rating (
  PRIMARY KEY (id_Movies_1, id_Reviewer_1, une_date  ),
  id_Movies_1   text NOT NULL,
  id_Reviewer_1 text NOT NULL,
  une_date        text NOT NULL,
  id_Movies_2   text,
  id_Reviewer_2 text,
  stars           text
);

CREATE TABLE Reviewer (
  PRIMARY KEY (id_Reviewer  ),
  id_Reviewer   text NOT NULL,
  name          text
);

ALTER TABLE Rating ADD FOREIGN KEY (id_Reviewer_1) REFERENCES Reviewer (id_Reviewer  );
ALTER TABLE Rating ADD FOREIGN KEY (id_Movies_1) REFERENCES Movies (id_Movies  );


-- 1 Contrainte pour éviter les doublons de titre et d'année :
ALTER TABLE movies
ADD CONSTRAINT unique_movie_title_year UNIQUE (title_movie, year_movie);

with open ("TP1_moviedata.sql","r") as fichier : %sql{{fichier.read()}}


/*1*/
select id_Reviewer
from Reviewer
where name LIKE 205

SELECT title
FROM Movies
/* WHERE */


SELECT title
FROM Movies
ORDER BY ASC


SELECT id_Movies
FROM Rating
WHERE name_reviewer LIKE Steven Spielberg


SELECT title
FROM Rating
WHERE id_Reviewer LIKE NULL


-- 2.1
SELECT name_reviewer
FROM reviewers
WHERE id_reviewer = 205;

SELECT title_movie
FROM movies;


SELECT title_movie
FROM movies
ORDER BY title_movie;

SELECT title_movie
FROM movies
WHERE director_movie = 'Steven Spielberg';


SELECT title_movie
FROM movies
WHERE director_movie IS NULL;


CREATE VIEW v_detail_evaluations AS
SELECT r.name_reviewer, m.title_movie, ra.stars_rating
FROM reviewers r
JOIN ratings ra ON r.id_reviewer = ra.id_reviewer
JOIN movies m ON ra.id_movie = m.id_movie;


SELECT DISTINCT m.year_movie
FROM movies m
JOIN ratings ra ON m.id_movie = ra.id_movie
WHERE ra.stars_rating IN (4, 5)
ORDER BY m.year_movie;



SELECT
FROM
WHERE


SELECT
FROM
WHERE

/*Exo 3 */

SELECT r.name_reviewer, m.title_movie, e.stars_rating AS note
FROM ratings e
JOIN movies m ON e.id_movie = m.id_movie
JOIN reviewers r ON e.id_reviewer = r.id_reviewer
WHERE e.stars_rating = (SELECT MIN(stars_rating) FROM ratings);

/*exo 4*/

SELECT r.name_reviewer, m.title_movie, e.stars_rating AS evaluations,
       AVG(e.stars_rating) OVER(PARTITION BY r.name_reviewer) AS "moyenne des notes de cet évaluateur"
FROM ratings e
JOIN movies m ON e.id_movie = m.id_movie
JOIN reviewers r ON e.id_reviewer = r.id_reviewer;

/*exo 5*/