#!/bin/bash
# Script pour importer des fichiers CSV dans PostgreSQL

# Tableau contenant les chemins des fichiers CSV
FILES=(
  "/home/adam/Documents/forgeLyon1/lifbda/tp/tpIngenerie/isere.csv"
  "/home/adam/Documents/forgeLyon1/lifbda/tp/tpIngenerie/loiret.csv"
  "/home/adam/Documents/forgeLyon1/lifbda/tp/tpIngenerie/rhone.csv"
)

# Connexion et importation
for FILE in "${FILES[@]}"; do
  echo "Importation de $FILE..."
  psql -h bd-pedago.univ-lyon1.fr -U p2110686 -d p2110686 -c "\COPY elections_brut FROM '$FILE' WITH CSV HEADER DELIMITER ','"
  if [ $? -eq 0 ]; then
    echo "Importation de $FILE réussie."
  else
    echo "Erreur lors de l'importation de $FILE."
  fi
done
