--3 
-- 4 
EXPLAIN (analyse,buffers) 
SELECT DISTINCT nump,nomp
FROM optimisation.produits
ORDER BY nomp
;

-- 6 
EXPLAIN (analyse,buffers) 
SELECT COUNT(*) AS nombre
FROM optimisation.commandes
WHERE numc IN 
	(select numc From optimisation.clients); 
EXPLAIN (analyse,buffers)
SELECT numc, COUNT(*) AS nombre 
FROM optimisation.commandes
GROUP BY numc;

-- 7 
EXPLAIN (analyse,buffers)
SELECT numc as "nom du client" , datecom as "date de la commande" 
FROM optimisation.commandes;



-- une autre version syntaxique de jointure
EXPLAIN (analyse,buffers)
SELECT clients.nomc as "nom du client" , commandes.datecom as "date de la commande"
FROM optimisation.clients
JOIN optimisation.commandes ON clients.numc = commandes.numc;

-- une autre version syntaxique de jointure
EXPLAIN (analyse,buffers)
SELECT clients.nomc as "nom du client" , commandes.datecom as "date de la commande"
FROM optimisation.clients, optimisation.commandes
WHERE clients.numc = commandes.numc;

-- une autre version syntaxique de jointure
EXPLAIN (analyse,buffers)
SELECT clients.nomc as "nom du client" , commandes.datecom as "date de la commande"
FROM optimisation.clients
JOIN optimisation.commandes USING (numc);




